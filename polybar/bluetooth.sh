#!/bin/bash

icon_enabled=""
icon_disabled=""
status=`systemctl is-active bluetooth.service`
device=`echo info |bluetoothctl |grep '^Device*'|wc -c`

if [ $status == "active" ]
then
    if [ $device != 0 ]
        then 
        echo "$icon_enabled  100%"
    else
        echo "$icon_enabled  0%"
    fi
else
    echo "$icon_disabled  0%"
fi

